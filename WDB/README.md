# WebDev Bootcamp

Trainig bits of code I write while following [this](https://www.udemy.com/the-web-developer-bootcamp/) and [this](https://www.udemy.com/the-advanced-web-developer-bootcamp/) Web Developer course on Udemy.

This makes use of [Bootstrap](getbootstrap.com) and consequently of [jQuery](jquery.com) and [Popper.js](popper.js.org).
